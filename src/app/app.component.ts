import { Component } from '@angular/core';
import { getLocaleEraNames } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task-twentynine';

  public text: string = 'This is not a auto generated text, this was manually typed by former soviet workers in Estonia.';

  public number: number = 42;

  public list: string[] = [
    'Start a revolution',
    'Liberate the workers',
    'Create the soviet union'
  ];

  user = { age: 'Immortal Entity', firstname: 'Reanu', lastname: 'Keeves'};
}


